
import 'package:flutter/material.dart';
import 'package:project_toro/routes/routes.dart';



void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      color: const Color.fromARGB(255, 231, 231, 230),
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      // initialRoute: 'login_view',
      initialRoute: 'login_view',
      routes: appRoutes,
       //theme: ThemeData(
        // primarySwatch: Colors.deepOrange,
      // ),
       
    );
  }
}
