import 'package:flutter/material.dart';

final ButtonStyle buttonAceptar = ElevatedButton.styleFrom(
  primary: Color(0xff00003D),
  //onPrimary: Colors.grey[300],
  minimumSize:const Size(60, 36),
  padding:const EdgeInsets.symmetric(horizontal: 100.0),
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(30.0),
  ),

  
);