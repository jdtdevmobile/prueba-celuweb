import 'package:flutter/material.dart';

class SelectorFecha extends StatefulWidget {
  const SelectorFecha({Key? key}) : super(key: key);

  // const SelectorFecha({Key? key}) : super(key: key);
  @override
  State<SelectorFecha> createState() => _SelectorFechaState();
}

class _SelectorFechaState extends State<SelectorFecha> {
  var _currentSelectedDate;

  //1. LLamar al Picker
  void callDatePicker() async {
    var selectedDate = await getDatePickerWidget();
    setState(() {
      _currentSelectedDate = selectedDate;
    });
  }

  Future<DateTime?> getDatePickerWidget() {
    return showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2022),
      lastDate: DateTime(2023),
      // builder: (context, child) {
      //   return Theme(data: ThemeData.dark(), child: child);
      //   },
    );
  }

//2.Crear Widget DatePicker.

  @override
  Widget build(BuildContext context) {
    //3. boton que lanza nuestro dialogo datePicker.
    return Scaffold(
      body: TextButton(
        onPressed: callDatePicker,
        child: const Text('Seleccionar fecha'),
      ),
    );
  }
}
