
import 'package:flutter/material.dart';

class ForgetPassword extends StatelessWidget {
  const ForgetPassword({key});

  @override
  Widget build(BuildContext context) {
    return Container(
              alignment: Alignment.centerRight,
              width: double.infinity,
              child: TextButton(
                  onPressed: () {},
                  child: const Text(
                    'Olvidaste Tu Contraseña?',
                    style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.white,
                        decoration: TextDecoration.underline),
                      )
                    ),

                );
  }
}