import 'package:flutter/material.dart';

final ButtonStyle raiserdButtonStyle = ElevatedButton.styleFrom(
  primary: const Color(0xff00003D),
  onPrimary: Colors.grey[300],
  //minimumSize:const Size(88, 36),
  padding:const EdgeInsets.symmetric(horizontal:40.0, vertical: 15),
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(30.0),
  ),

  
);