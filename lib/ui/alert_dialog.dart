import 'package:flutter/material.dart';
import 'package:project_toro/ui/button_aceptar_alert.dart';
import 'package:project_toro/ui/date_picker.dart';

import 'picker_actual.dart';

Future<void> showMyDialog(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        //title: const Text('Fecha de pedido'),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(21.0),
        ),
        content: SizedBox(
          //width: 250,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(height: 40.0),
              Container(
                height: 90.0,
                width: double.infinity,
               // padding:
                 //   const EdgeInsets.symmetric(vertical: 10, horizontal: 1),
                // decoration: BoxDecoration(
                //   //color: Colors.blue,
                //   borderRadius: BorderRadius.circular(41.0),
                //   border: Border.all(color: Colors.grey),
                //   // shape: BoxShape.circle,
                // ),
                child: const SeleccionarFecha(),
                // child: Row(
                //   // crossAxisAlignment: CrossAxisAlignment.center,
                //   //mainAxisAlignment: MainAxisAlignment.center,
                //   children: [
                //     const Text('Fecha  pedido'),
                //     const SizedBox(width: 100.0),
                //     IconButton(
                //       onPressed: () async {
                //         print('lalallalalalal');
                //         // const SelectorFecha();
                //       const   SeleccionarFecha();
                //         // showDatePicker(
                //         //   context: context,
                //         //   initialDate: DateTime.now(),
                //         //   firstDate: DateTime(2022),
                //         //   lastDate: DateTime(2023),

                //         //   // builder: (context, child) {
                //         //   //   return Theme(data: ThemeData.dark(), child: child);
                //         //   //   },
                //         // );
                //       },
                //       icon: const Icon(Icons.date_range),
                //     )
                //   ],
                // ), //
              ),
              const SizedBox(height: 21.0),
              Container(
                height: 90.0,
                width: double.infinity,
                padding:
                    const EdgeInsets.symmetric(vertical: 20, horizontal: 1),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(21.0),
                  border: Border.all(color: Colors.grey),
                  // shape: BoxShape.circle,
                ),
                child: const Text('Detalle del pedido'),
              ),

              // const SizedBox(height: 10.0),
              const SizedBox(height: 21.0),
              ElevatedButton(
                style: buttonAceptar,
                //Aqui le pone la forma redondeada al boton o sea el style.
                child: const Text(
                  'Aceptar',
                  style: TextStyle(color: Color(0xffEFDA30)),
                ),
                onPressed: () {},
              ),
            ],
          ),
        ),
      );
    },
  );
}
