import 'package:flutter/material.dart';


final ButtonStyle styleButtonContador = ElevatedButton.styleFrom(
  primary: Color.fromARGB(255, 230, 230, 236),
  onPrimary: Colors.grey[300],
  minimumSize:const Size(88, 36),
  padding:const EdgeInsets.symmetric(horizontal: 110.0, vertical: 15.0),
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(30.0),
  ),

  
);