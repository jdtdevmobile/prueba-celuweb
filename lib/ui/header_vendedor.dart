import 'package:flutter/material.dart';

class HeaderVendedor extends StatelessWidget {
  const HeaderVendedor({Key? key, this.nombreDelCliente}) : super(key: key);
  final String? nombreDelCliente;

  @override
  Widget build(BuildContext context) {
    return Container(
                height: 130,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(21.0), bottomRight: Radius.circular(21.0) ),
                  boxShadow:  [
                      BoxShadow(
                        color: Colors.black,
                        offset: Offset(1.5, 0.3),
                        blurRadius: 2
                      )
                  ]
                ),
                child: Row(
               
                  children: [
                    
                  IconButton(onPressed: (){
                    Navigator.pop(context);
                          }, 
                          icon: const Icon(Icons.arrow_back_ios_new)
                  ),
                
        //   grande    CircleAvatar(
        //             radius: (40),
        //             backgroundColor: Colors.white,
        //             child: ClipRRect(
        //           //borderRadius:BorderRadius.circular(80),
        //             child: Image.asset("assets/image/Logo_azulg.png"),
        //     )
        // ),
                    CircleAvatar(
                    radius: (28),
                    backgroundColor: Color.fromARGB(255, 235, 232, 232),
                    child: ClipRRect(
                    //borderRadius: BorderRadius.circular(100),
                    child: Image.asset("assets/image/Logo_azul.png"),
                    )
                  ),

                  const SizedBox(width: 15.0,),
                  
                  Column (
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      
                      //Text(vendedores[position]["nombre"]),
                            //Text(vendedores[position]["empresa"]),
                      Text('Hola ${nombreDelCliente}', 
                        style: const TextStyle(
                        fontSize: 17.0, color: Color(0xff00003D), fontWeight: FontWeight.bold ),
                        ),
                      const Text('Bienvenido'),

                    ],
                  ),
                 const SizedBox(width: 80,),

                    ClipRRect(
                                     // borderRadius: BorderRadius.circular(41),
                    child: Image.asset("assets/image/Icono_carrito_compras.png"),
                    ),


                 
                ]),
              );
  }
}