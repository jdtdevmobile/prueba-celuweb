 import 'package:flutter/material.dart';

class SeleccionarFecha extends StatefulWidget {
  const SeleccionarFecha({Key? key}) : super(key: key);

  @override
  State<SeleccionarFecha> createState() => _SeleccionarFechaState();
}

class _SeleccionarFechaState extends State<SeleccionarFecha> {
    TextEditingController _inputFieldDateController = TextEditingController();
   String fechaSeleccionada="";
  @override
  Widget build(BuildContext context) {
    return Material(
      child: TextField(
        enableInteractiveSelection: false,
    
        controller: _inputFieldDateController,
        decoration: InputDecoration(
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20.0)),
          hintText: 'Fecha del pedido',
          labelText: 'Fecha del pedido',
          suffixIcon:const Icon(Icons.perm_contact_calendar),
          icon:const Icon(Icons.calendar_today),
        ),
    
       
        onTap: () {
          FocusScope.of(context).requestFocus( FocusNode());
          _selecDate();
        },
      ),
    );
  }
    _selecDate() async {
  DateTime?  picked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2010),
      lastDate: DateTime(2025),
      // locale:const Locale('es', 'ES'),
    );

    if (picked != null) {
      setState(() {
        fechaSeleccionada = picked.toString();
        _inputFieldDateController.text = fechaSeleccionada;
      });
    }
  }
}

// Widget _crearFecha(BuildContext context) {
//   TextEditingController _inputFieldDateController = TextEditingController();
//    String fechaSeleccionada="";
//   //Metodo de selecion de fecha
//   _selecDate(BuildContext context) async {
//   DateTime?  picked = await showDatePicker(
//       context: context,
//       initialDate: DateTime.now(),
//       firstDate: DateTime(2010),
//       lastDate: DateTime(2025),
//       locale:const Locale('es', 'ES'),
//     );

//     // if (picked != null) {
//       // setState(() {
//       //   _fecha = picked.toString();
//       //   _inputFieldDateController.text = _fecha;
//       // });
//     // }
//   }
//   return TextField(
//       enableInteractiveSelection: false,

//       controller: _inputFieldDateController,
//       decoration: InputDecoration(
//         border: OutlineInputBorder(
//             borderRadius: BorderRadius.circular(20.0)),
//         hintText: 'Fecha de nacimiento',
//         labelText: 'Fecha nacimiento',
//         suffixIcon:const Icon(Icons.perm_contact_calendar),
//         icon:const Icon(Icons.calendar_today),
//       ),

     
//       onTap: () {
//         FocusScope.of(context).requestFocus( FocusNode());
//         _selecDate(context);
//       },
//     );
//   }
