import 'package:flutter/material.dart';


final ButtonStyle guardarpedidoButtonStyle = ElevatedButton.styleFrom(
  primary: Color(0xff00003D),
  onPrimary: Colors.grey[300],
  minimumSize:const Size(88, 36),
  padding:const EdgeInsets.symmetric(horizontal: 110.0, vertical: 20),
  shape: RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(30.0),
  ),

  
);