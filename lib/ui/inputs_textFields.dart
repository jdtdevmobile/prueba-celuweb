import 'package:flutter/material.dart';

class InputDecorations {
  static InputDecoration authInputDecoration(
      {required String hintText, ClipRRect? prefixIcon}) {
    return InputDecoration(
        prefixIcon: ClipRRect(
          child: prefixIcon,
        ),
        enabledBorder: const UnderlineInputBorder(
            borderSide: BorderSide(color: Colors.white)),
        hintText: hintText,
        hintStyle: const TextStyle(color: Colors.white));
  }
}
