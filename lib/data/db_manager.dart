import 'dart:io';

import 'package:flutter/services.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;

class DbManager {
  static Database? db;
  Future<Database> initDB() async {
    var dbDir = await getDatabasesPath();
    var dbPath = p.join(dbDir, "celuwebdb.db");
    await deleteDatabase(dbPath);

    ByteData data = await rootBundle.load("assets/celuwebdb.db");
    List<int> bytes =
        data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
    await File(dbPath).writeAsBytes(bytes);
    return db = await openDatabase(dbPath, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE Pedidos(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          cliente TEXT,
          producto TEXT, 
          precio REAL, 
          vendedor INTEGER,
          fecha TEXT
         )
         ''');
  }

  Future<Database> get getDatabase async {
    if (db != null) {
      return db!;
    } else {
      db = await initDB();
    }
    return db!;
  }
}
