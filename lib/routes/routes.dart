
import 'package:flutter/cupertino.dart';
import 'package:project_toro/views/login_view.dart';
import 'package:project_toro/views/vendedores_view.dart';

final Map<String, Widget Function(BuildContext)> appRoutes={
'login_view'       : (_) => const LoginView(),
'view_vendedores' : (_) => const VendedoresView(),

} ;