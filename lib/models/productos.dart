

class Productos {
  final String? nombre;
  final double? precio;
  final String? vendedor;

  Productos({
    this.nombre,
    this.precio,
    this.vendedor,
  });

  Productos.fromJson(Map<String, dynamic> json)
      : nombre = json['Nombre'],
        precio = json['Precio'],
        vendedor = json['Vendedor'];
}
