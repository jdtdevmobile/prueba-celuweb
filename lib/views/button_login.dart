import 'package:flutter/material.dart';

import 'package:project_toro/ui/buttom2_constant..dart';
//import 'package:project_toro/views/login_view.dart';

class ButtonLogin extends StatelessWidget {
  
  const ButtonLogin({Key? key, required this.onPressed}) : super(key: key);

   final ValueGetter<String> onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(100.0)),
      child: ElevatedButton(
        style: loginButtonStyle2,
        onPressed: () {
           //LogView.login();
        },
        child: const Text('Iniciar Sesión',
            style: TextStyle(
                color: Color(0xff00003D),
                fontSize: 18,
                fontWeight: FontWeight.bold)),
      ),
    );
  }
}

