import 'package:flutter/material.dart';
import 'package:project_toro/ui/etiqueta_catalogo_producto.dart';
import 'package:project_toro/views/detalle_productos_view.dart';
import 'package:sqflite/sqflite.dart';

class ProductosView extends StatefulWidget {
  const ProductosView({Key? key, this.db, this.nombreDelCliente})
      : super(key: key);
  final Database? db;
  final String? nombreDelCliente;

  @override
  State<ProductosView> createState() => _ProductosViewState();
}



/////  puente widget
class _ProductosViewState extends State<ProductosView> {
  @override
  void initState() {
    getProductos();
    super.initState();
    // initDB();
  }

  List<Map<String, dynamic>> productos = [];
  //List<Map> clientes = [];
  List<Map> vendedores = [];

  void getProductos() async {
    if (widget.db != null) {
      productos = await widget.db!.query("ProductosCatalogo");
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     
      body: Stack(
        children:[ 
          Container(
            color: Color.fromARGB(255, 231, 231, 230),
            child: Column(
            children: [     
                Container(
                  height: 130,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(21.0), 
                      bottomRight: Radius.circular(21.0) ),
                      boxShadow:  [
                        BoxShadow(
                          color: Colors.black38,
                          offset: Offset(0.0, 1.5),
                          blurStyle: BlurStyle.outer,
                          blurRadius: 7
                        )
                    ]
                  ),
                  child: Row(
                 
                    children: [
                      
                    IconButton(onPressed: (){
                      Navigator.pop(context);
                            }, 
                            icon: const Icon(Icons.arrow_back_ios_new)
                    ),
                  
        
                      CircleAvatar(
                      radius: (28),
                      backgroundColor: Color.fromARGB(255, 235, 232, 232),
                      child: ClipRRect(
                      //borderRadius: BorderRadius.circular(100),
                      child: Image.asset("assets/image/Logo_azul.png"),
                      )
                    ),

                    const SizedBox(width: 15.0,),
                    
                    Column (
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        
                        //Text(vendedores[position]["nombre"]),
                              //Text(vendedores[position]["empresa"]),
                        Text('Hola ${widget.nombreDelCliente}', 
                          style: const TextStyle(
                          fontSize: 17.0, 
                          color: Color(0xff00003D), 
                          fontWeight: FontWeight.bold ),
                          ),
                        const Text('Bienvenido'),

                      ],
                    ),
                      const SizedBox(width: 80,),

                      ClipRRect(
                   // borderRadius: BorderRadius.circular(41),
                      child: Image.asset("assets/image/Icono_carrito_compras.png"),
                      ),


                   
                  ]),
                ),

            const SizedBox(height: 30.0,),  
              
              Container(
                color: const Color.fromARGB(255, 231, 231, 230),
                padding: const EdgeInsets.only(left: 30.0),
                alignment: Alignment.topLeft,
                child: ElevatedButton(
                  style: catalogoEtiqueta,
                  onPressed:  (){}, 
                  child: const Text('Catalogo', 
                  style: TextStyle(color: Color(0xffEFDA30)),)),
        ),
      
              Expanded(
                child: Container(
                  color: const Color.fromARGB(255, 231, 231, 230),
                  padding: const EdgeInsets.all(10.0),
                  //child: ListView.builder(
                    child: GridView.builder(
                      
                    shrinkWrap: true,
                    itemCount: productos.length,
                    gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    mainAxisExtent: 280,
                    
                    //childAspectRatio: 2,
                    crossAxisCount: 2
                    ),
                    itemBuilder: (_, position) {
                      return GestureDetector(
                        onTap: () {
                          
                          String nombre     = '${productos[position]["Nombre"]}';
                          String precio     = '${productos[position]["Precio"]}';
                          String vendedor   = '${productos[position]["Vendedor"]}';
                          String codigo     = '${productos[position]["Codigo"]}';
                          
                          List<Map<String, Object>> listDeProductos = [
                    
                            {
                              "Nombre"    : '${productos[position]["Nombre"]}',
                              "Precio"    : '${productos[position]["Precio"]}',
                              "Vendedor"  : '${productos[position]["Vendedor"]}',
                              "Codigo"    : '${productos[position]["Codigo"]}'
                            }
                          ];
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (BuildContext context) => DetalleProductoScreen(
                                db: widget.db,
                                nombreDelCliente: widget.nombreDelCliente,
                                listaDeProductos: listDeProductos,
                              ),
                            ),
                          );
                        },//copi and page code
                        child: Container(
                        //color: Colors.red,
                        margin: const EdgeInsets.all(8.0),
                        //height: 10,
                        //width: 25,
                        decoration:  BoxDecoration(
                           color: Colors.white,  
                           borderRadius: BorderRadius.circular(10.0),
                           boxShadow: const [
                              BoxShadow(
                                  color: Colors.black38,
                                  offset: Offset(0.0, 1.5),
                                  blurStyle: BlurStyle.outer,
                                  blurRadius: 2.5)
                            ]),
                                    
                        padding: const EdgeInsets.all(16),
                        child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                          // Text('Cliente: ${widget.cliente}'),
                         const SizedBox(height:20),
                          Center(
                            child: ClipRRect(
                              child: Image.asset("assets/image/No_Image_Grid.png"),
                            ),
                          ),
                          const SizedBox(height: 15),
                          Text('${productos[position]["Nombre"]}', 
                          style: const TextStyle( fontWeight: FontWeight.bold, color: Color(0xff00003D) ),
                          ),
                          Text('${productos[position]["Vendedor"]}'),
                          Text('\$ ${productos[position]["Precio"]}', style:  TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold, color: Color(0xff00003D) ),),
                          const Spacer(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: const [
                              
                               
                               CircleAvatar(
                                radius: 10.0,
                                child: Text('—', style: TextStyle(fontWeight: FontWeight.bold),),
                                backgroundColor: Color.fromARGB(255, 235, 232, 232), 
                              ),
                              //SizedBox(width: 15,),
                              Text('2'),
                               CircleAvatar(
                            radius: 10.0,
                            child: Text('+',style: TextStyle(fontWeight: FontWeight.bold),),
                            backgroundColor: Color.fromARGB(255, 235, 232, 232), 
                          )
                            ],
                          )
                          // IconButton(onPressed: (){}, 
                          // icon: Icon(Icons.)
                          //const SizedBox(height: 21.0),
                          ],
                        ),
                                ),
                                
                              ) ;
                    },
                  ),
                ),
              ),
            ],
        ),
          ),
      ]),
    );
  }
}