import 'package:flutter/material.dart';
import 'package:project_toro/ui/alert_dialog.dart';
import 'package:project_toro/ui/buttom_guardar_pedido.dart';
import 'package:project_toro/ui/etiqueta_catalogo_producto.dart';
import 'package:sqflite/sqflite.dart';


class ProductosGuardadosView extends StatefulWidget {
  ProductosGuardadosView(
     // {Key? key,
     // this.db,
      //this.nombreDelCliente,
      //required this.listaDeProductos})
      //: super(key: key);
  //Database? db;
 // final String? nombreDelCliente, 
  {Key? key,  required this.lista, this.nombreDelCliente}) : super(key: key);
 final List<Map<String, Object? >> lista;
        Database? db;
        final String? nombreDelCliente;
  //final List<Map<String, Object>>? listaDeProductos;

  @override
  State<ProductosGuardadosView> createState() =>
      _ProductosGuardadosViewState();
}

class _ProductosGuardadosViewState
    extends State<ProductosGuardadosView> {
  @override
  Widget build(BuildContext context) {
    

    // final DateTime now = DateTime.now();
    // final DateFormat formatter = DateFormat('yyyy-MM-dd');
    // final String formatted = formatter.format(now);

    return Scaffold(
      
      body: Stack(
        children:[ 
            Container(
              color: const Color.fromARGB(255, 231, 231, 230),
              child: Column(
               children: [
                    Container(
                    height: 130,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(21.0), bottomRight: Radius.circular(21.0) ),
                      boxShadow:  [
                          BoxShadow(
                            color: Colors.black38,
                            offset: Offset(0.0, 1.5),
                            blurStyle: BlurStyle.outer,
                            blurRadius: 7.0
                          )
                      ]
                    ),
                    child: Row(
                   
                      children: [
                        
                      IconButton(onPressed: (){
                        Navigator.pop(context);
                              }, 
                              icon: const Icon(Icons.arrow_back_ios_new)
                      ),
                    
          
                        CircleAvatar(
                        radius: (28),
                        backgroundColor: const Color.fromARGB(255, 235, 232, 232),
                        child: ClipRRect(
                        //borderRadius: BorderRadius.circular(100),
                        child: Image.asset("assets/image/Logo_azul.png"),
                        )
                      ),
      
                      const SizedBox(width: 15.0,),
                      
                      Column (
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          
                          //Text(vendedores[position]["nombre"]),
                                //Text(vendedores[position]["empresa"]),
                          Text('Hola ${widget.nombreDelCliente}', 
                            style: const TextStyle(
                            fontSize: 17.0, color: Color(0xff00003D), fontWeight: FontWeight.bold ),
                            ),
                          const Text('Bienvenido'),
      
                        ],
                      ),
                     const SizedBox(width: 130),
      
                        ClipRRect(
                                         // borderRadius: BorderRadius.circular(41),
                        child: Image.asset("assets/image/Icono_carrito_compras.png"),
                        ),
      
      
                     
                    ]),
                  ),
      
              const SizedBox(height: 20.0,),  
                Container(
                  padding: const EdgeInsets.only(left: 30.0),
                  alignment: Alignment.topLeft,
                  child: ElevatedButton(
                    style: catalogoEtiqueta,
                    onPressed:  (){}, 
                    child: const Text('Carrito', style: TextStyle(color: Color(0xffEFDA30)),)),
                  ),
               const   Divider(color: Colors.black,),
        
              Expanded(
                child: Container(
                  
                  padding: const EdgeInsets.all(16.0),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: widget.lista.length,
                    
                    itemBuilder: (_, position) {
                      final lista = widget.lista;
                     
                  
                      return GestureDetector(
                        onTap: () {},
                        child: Container(
                          height: 140.0,
                          width: 300,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            //border: Border.all(color: Colors.black, width: 2.0),
                            borderRadius: BorderRadius.circular(21.0),
                            boxShadow: const [
                              BoxShadow(
                                color: Colors.black38,
                                offset: Offset(0.0, 1.5),
                                blurStyle: BlurStyle.outer,
                                blurRadius: 7.0
                              )
                            ]
                          ),
                          margin: const EdgeInsets.all(2.0),
                          padding: const EdgeInsets.all(16),
                          
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              ClipRRect(
                                child: Image.asset('assets/image/No_Image_Carritop.png'),
                              ),
                              const SizedBox(width: 5.0,),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: 
                                    [
                                  Text('${lista[position]["producto"]}', style: const TextStyle(fontWeight: FontWeight.bold, color: Color(0xff00003D) ), ),      
                                  const SizedBox(height: 5.0),
                                  Text('Cliente: ${lista[position]["cliente"]}'),
                                  const SizedBox(height: 5.0),
                                  //Text('${lista[position]["precio"]}', style: const TextStyle(color: Color(0xff00003D) ),),
                                  Text('Vendedor: ${lista[position]["vendedor"]}'),
                                  const SizedBox(height: 5.0),
                                  Text('fecha del pedido: ${lista[position]["fecha"]}'), 
                                  const SizedBox(height: 7.0),
                                  
                                  
                              Row(     
                            
                              children: const [

                               CircleAvatar(
                                radius: 10.0,
                                child: Text('—', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff00003D) ),),
                                backgroundColor: Colors.grey, 
                              ),
                              SizedBox(width: 35.0),
                              Text('2'),
                              SizedBox(width: 35.0),
                               CircleAvatar(
                            radius: 10.0,
                            child: Text('+',style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xff00003D)),),
                            backgroundColor: Colors.grey, 
                              )
                          
                            ],
                            
                          ),        
                        ],
                      ),
                          Container(
                            alignment: Alignment.topRight,
                            height: 40,
                            width: 80,
                            
                            child: Text('\$${lista[position]["precio"]}', 
                            style: const TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: Color(0xff00003D), )
                            ),
                          )       
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
             const Divider(
                color: Colors.black,
              ),
              const SizedBox(height: 5.0,),
              Container(
                padding: const  EdgeInsets.only(bottom: 10.0),
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(100.0)
                  ),
                
                child: ElevatedButton(
                style: guardarpedidoButtonStyle,
                onPressed: () {
                  showMyDialog(context);
                },
                child: const Text('Guardar Pedido',
                    style: TextStyle(color: Color(0xffEFDA30)
                    )
                  ),
              ),
            )
          ],
        ),
            ),
        ]
      ),
    );
  }
}

class SelectorFecha extends StatefulWidget {
 // const SelectorFecha({Key? key}) : super(key: key);
  @override
  State<SelectorFecha> createState() => _SelectorFechaState();
}

class _SelectorFechaState extends State<SelectorFecha> {
  var currentSelectedDate;
  
  //1. LLamar al Picker
  void callDatePicker() async{
    var selectedDate = await getDatePickerWidget();
    setState(() {
      currentSelectedDate = selectedDate;
      
    });
  }
  
Future <DateTime?> getDatePickerWidget() {
  return showDatePicker(
    context: context, 
    initialDate: DateTime.now(), 
    firstDate: DateTime(2022), 
    lastDate: DateTime(2023),
    // builder: (context, child) {
    //   return Theme(data: ThemeData.dark(), child: child);
    //   },
    );
    
  }
  

//2.Crear Widget DatePicker.

  @override
  Widget build(BuildContext context) {
    print('Fechaaaaaaaaaa   ${currentSelectedDate}');
    //3. boton que lanza nuestro dialogo datePicker.
    return Scaffold(
        body: Center(
          child: Container(
          child: TextButton(
                  onPressed: callDatePicker, 
                  child: Text('Seleccionar fecha')),

          ),
        ),
    );
    
  }
}