import 'package:flutter/material.dart';
import 'package:project_toro/views/productos_view.dart';
import 'package:sqflite/sqflite.dart';

class ClientesView extends StatefulWidget {
  const ClientesView({Key? key, this.db}) : super(key: key);
  final Database? db;

  @override
  State<ClientesView> createState() => _ClientesViewState();
}

class _ClientesViewState extends State<ClientesView> {
  @override
  void initState() {
    getClientes();
    super.initState();
    // initDB();
  }

  List<Map> clientes = [];

  void getClientes() async {
    if (widget.db != null) {
      clientes = await widget.db!.rawQuery("SELECT * FROM Clientes");
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color.fromARGB(255, 231, 231, 230),
      padding: const EdgeInsets.all(16.0),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: clientes.length,
        itemBuilder: (_, position) {
          
          String nombreDelCliente = clientes[position]["Nombre"];
          return GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => ProductosView(
                          db: widget.db, nombreDelCliente: nombreDelCliente)));
            },
            child: Container(
              
              decoration: BoxDecoration(
                color: Colors.white,
               // border: Border.all(color: Colors.black, width: 2.0),
               boxShadow: const [
                  BoxShadow(
                    color: Colors.black38,
                    offset: Offset(0.0, 1.5),
                    blurStyle: BlurStyle.outer,
                    blurRadius: 2.5,
                    
                    //spreadRadius:0.5
                  )
               ],
                borderRadius: BorderRadius.circular(21.0),
              ),
              margin: const EdgeInsets.all(11),
              padding: const EdgeInsets.all(16),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children:  [
                  Text(clientes[position]["Nombre"], 
                  style: const TextStyle(fontSize: 17.0, color: Color(0xff00003D), fontWeight: FontWeight.bold ),
                  ),
                  Text(clientes[position]["Ciudad"]),
                  Text(clientes[position]["Direccion"]),
                  //const SizedBox(height: 20.0),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}