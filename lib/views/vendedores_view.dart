import 'package:flutter/material.dart';
import 'package:project_toro/ui/etiqueta_clientes.dart';
import 'package:project_toro/views/clientes_view.dart';
import 'package:project_toro/views/views.dart';
import 'package:sqflite/sqflite.dart';

class VendedoresView extends StatefulWidget {
  const VendedoresView({Key? key, this.db, this.nombreDelCliente})
      : super(key: key);
  final Database? db;
  final String? nombreDelCliente;

  //widget.db hago uso de la base de datos que envie anteriormente como parametro al presionar el boton de login

  @override
  State<VendedoresView> createState() => _VendedoresViewState();
}

class _VendedoresViewState extends State<VendedoresView> {
  List<Map> vendedores = [];
  void getVendedores() async {
    if (widget.db != null) {
      vendedores = await widget.db!.rawQuery("SELECT * FROM Vendedor");
      //vendedores = await widget.db!.query("Vendedor"); funciona correctamente
      setState(() {});
      Text(vendedores.map((e) => ['nombre']).toString());
    }
  }

  @override
  void initState() {
    getVendedores();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    //  Database? db = ModalRoute.of(context)!.settings.arguments as Database?;
    return Scaffold(
      body: Stack(
        children: [
          Column(children: [

            
            Container(
              color: Color.fromARGB(255, 231, 231, 230),
              padding: const EdgeInsets.all(16.0),
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: vendedores.length,
                  itemBuilder: (_, position) {
                    return Container(
                      height: 200,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          //border: Border.all(color: Colors.black, width: 2.0),
                          borderRadius: BorderRadius.circular(21.0),
                          boxShadow: const [
                            BoxShadow(
                                color: Colors.black38,
                                offset: Offset(0.0, 1.5),
                                blurStyle: BlurStyle.outer,
                                blurRadius: 2.5)
                          ]),
                            margin: const EdgeInsets.all(11),
                            padding: const EdgeInsets.all(16),
                    child: Column(
                      
                      children: [
                        Row(
                          children:  [
                            GestureDetector(
                              onTap: (){

                                //AlertDialog(title:)
                                //Navigator.of(context).pop(LoginView);
                              },
                              child: ClipRRect(
                                child: Image.asset("assets/image/Icono_cerrar_sesion.png"),
                              ),
                            ),
                             const SizedBox(width: 235,),
                            ClipRRect(
                              child: Image.asset("assets/image/Icono_carrito_compras.png"),
                            ),
                            
                          ],
                        ),

                        const SizedBox(width: 25, height: 11,),
                        Row(
                          children: [
                            Center(
                                child: Column(
                                  children: [
                                     CircleAvatar(
                                          radius: (40),
                                          backgroundColor: Color.fromARGB(255, 235, 232, 232),
                                          child: ClipRRect(
                                          //borderRadius: BorderRadius.circular(100),
                                          child: Image.asset("assets/image/Logo_azulg.png"),
                                        )
                                      ),
                                      const SizedBox(height: 5.0,),
                                    Text(vendedores[position]["nombre"]),
                                    Text(vendedores[position]["empresa"])
                                    //Text('Cliente: ${widget.nombreDelCliente}'),
                                    //Text(vendedores.map((e) => 'nombre').toString()),
                                  
                                  ],                              
                                ),
                              ),
                              const SizedBox(width: 25.0,), 
                              Column(
                                children: [
                                  
                                 Row(
                                   children: [
                                     Column(
                                       children: [
                                         ClipRRect(
                                        child: Image.asset("assets/image/Icono_perfil1.png"),
                                        ),
                                       const Text('Lorem \n 5000'),
                                     
                                       ],
                                     ),
                                    const SizedBox(width: 15,),
                                     Column(
                                       children: [
                                         ClipRRect(
                                            child: Image.asset("assets/image/Icono_perfil2.png"),
                                            ),
                                           const Text('Lorem \n 4500'),
                                       ],
                                     ),
                                       const SizedBox(width: 15,),
                                        Column(
                                          children: [
                                            ClipRRect(
                                            child: Image.asset("assets/image/Icono_perfil3.png"),
                                            ),
                                          const  Text('Lorem \n    46')
                                          ],
                                        ),
                                        
                                   ],
                                 ),
                                  

                                ],
                              ),
                          ],
                        ),
                      ],
                    ),
                      // );
                      //},
                    );
                  }),
            ),
           
           
           Container(
              color: const Color.fromARGB(255, 231, 231, 230),
              padding: const EdgeInsets.only(left: 25 ),
              alignment: Alignment.centerLeft,
              width: double.infinity,
              child: ElevatedButton(onPressed: () {},
              style: clientesEtiqueta,
              child:const Text('Clientes', style: TextStyle(color: Color(0xffEFDA30) ),)
               )
              ),



            Expanded(child: ClientesView(db: widget.db)),
          ]
        ),

          
          //
        ],
      ),
    );
  }
}
