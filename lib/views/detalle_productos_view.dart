import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:project_toro/ui/etiqueta_catalogo_producto.dart';
import 'package:project_toro/views/pedidos_guardados.dart';
import 'package:sqflite/sqflite.dart';

import '../ui/button_constan.dart';

class DetalleProductoScreen extends StatefulWidget {
  DetalleProductoScreen(
        {Key? key,
        this.db,
        this.nombreDelCliente,
        required this.listaDeProductos})
        : super(key: key);
        Database? db;
        final String? nombreDelCliente;
        final List<Map<String, Object>> listaDeProductos;
        

  @override
  State<DetalleProductoScreen> createState() =>
      _DetalleProductoScreenState();
}

class _DetalleProductoScreenState extends State<DetalleProductoScreen> {
  
  List<Map<String, Object? >> lista = [];
   var currentSelectedDate;

  @override
  Widget build(BuildContext context) {
    String codigo     = '';
    String producto   = '';
    String precio     = '';
    String vendedor   = '';
  

    final DateTime now          =   DateTime.now();
    final DateFormat formatter  =   DateFormat('yyyy-MM-dd');
    final String formatted      =   formatter.format(now);

    return Scaffold(
      
      body: Container(
        color: const Color.fromARGB(255, 231, 231, 230),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                  height: 130,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(21.0), bottomRight: Radius.circular(21.0) ),
                    boxShadow:  [
                        BoxShadow(
                          color: Colors.black38,
                          offset: Offset(0.0, 1.5),
                          blurStyle: BlurStyle.outer,
                          blurRadius: 7
                        )
                    ]
                  ),
                  child: Row(
                 
                    children: [
                      
                    IconButton(onPressed: (){
                      Navigator.pop(context);
                            }, 
                            icon: const Icon(Icons.arrow_back_ios_new)
                    ),
                  
                      CircleAvatar(
                          radius: (28),
                          backgroundColor: Color.fromARGB(255, 235, 232, 232),
                          child: ClipRRect(
                          //borderRadius: BorderRadius.circular(100),
                          child: Image.asset("assets/image/Logo_azul.png"),
                      )
                    ),

                    const SizedBox(width: 15.0,),
                    
                    Column (
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        
                        //Text(vendedores[position]["nombre"]),
                              //Text(vendedores[position]["empresa"]),
                        Text('Hola ${widget.nombreDelCliente}', 
                          style: const TextStyle(
                          fontSize: 17.0, color: Color(0xff00003D), fontWeight: FontWeight.bold ),
                          ),
                        const Text('Bienvenido'),

                      ],
                    ),
                   const SizedBox(width: 80),

                      GestureDetector(
                        onTap: (){

                          Navigator.push(
                          context,
                          MaterialPageRoute(
                          builder: (BuildContext context) => ProductosGuardadosView(lista: lista)
                          //builder: (BuildContext context) => SelectorFecha()
                        )
                      );

                        },
                        child: ClipRRect(
                                         // borderRadius: BorderRadius.circular(41),
                        child: Image.asset("assets/image/Icono_carrito_compras.png"),
                        ),
                      ),


                   
                  ]),
                ),

            const SizedBox(height: 30.0,),  
              Container(
                color: Color.fromARGB(255, 231, 231, 230),
                padding: const EdgeInsets.only(left: 30.0),
                alignment: Alignment.topLeft,
                child: ElevatedButton(
                  style: catalogoEtiqueta,
                  onPressed:  (){}, 
                  child: const Text('Detalle del producto', 
                  style: TextStyle(color: Color(0xffEFDA30)),)),
          ),
        

            Container(
             
              padding: const EdgeInsets.all(6.0),
              child: ListView.builder(
                shrinkWrap: true,
                // itemCount: widget.listaDeProductos?.length,
                itemCount: widget.listaDeProductos.length,
                itemBuilder: (_, position) {
                  final productos = widget.listaDeProductos;
                  codigo = '${productos[position]["Codigo"]}';
                  producto = '${productos[position]["Nombre"]}';
                  precio = '${productos[position]["Precio"]}';
                  vendedor = '${productos[position]["Vendedor"]}';

                  return GestureDetector(
                    onTap: () {},
                    child: Container(
                      height: 300,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        //border: Border.all(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(15.0),
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black38,
                            offset: Offset(0.0, 1.5),
                            blurStyle: BlurStyle.outer,
                            blurRadius: 7
                          )
                        ]
                      ),
                      margin: const EdgeInsets.only(bottom: 10, right: 32, left: 32),
                      padding: const EdgeInsets.all(25),
                      child: Center(
                        child: Column(
                          //crossAxisAlignment: CrossAxisAlignment.start,
                          children: 
                              [

                                ClipRRect(
                              child: Image.asset("assets/image/No_Image_Detalle.png"),
                            ),
                            //Text('Cliente: ${productos[position]["Nombre"]}'),
                            Text('Cliente: ${widget.nombreDelCliente}'),
                            const SizedBox(height: 5.0),
                            Text('Producto: ${productos[position]["Nombre"]}', 
                            style: const TextStyle( fontSize: 17.0, fontWeight: FontWeight.bold, color: Color(0xff00003D) ),
                            ),
                            
                            Text('Vendedor: ${productos[position]["Vendedor"]}'),
                            
                            Text('Precio: \$${productos[position]["Precio"]}', style: const TextStyle( fontSize: 20.0, fontWeight: FontWeight.bold, color: Color(0xff00003D) ),),
                            const SizedBox(height: 5.0),
                            Text('fecha del pedido:  $formatted'),
                            //const SizedBox(height: 21.0),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
            const SizedBox(height: 5.0),

            Container(
              padding: EdgeInsets.only(left: 50.0),
              alignment: Alignment.bottomLeft,
              child: const Text('Detalle', 
              style:  TextStyle( 
                fontSize: 20.0, 
                fontWeight: FontWeight.bold, color: Color(0xff00003D) ),
                )
              ),
            const SizedBox(height: 10.0),
                 
            Container(
              height: 95.0,
              width: 400.0,
              margin: const EdgeInsets.only(bottom: 10, right: 40, left: 40),
              padding: const EdgeInsets.all(25),
              decoration: BoxDecoration(
                        color: Colors.white,
                        //border: Border.all(color: Colors.black, width: 2.0),
                        borderRadius: BorderRadius.circular(15.0),
                        boxShadow: const [
                          BoxShadow(
                            color: Colors.black38,
                            offset: Offset(0.0, 1.5),
                            blurStyle: BlurStyle.outer,
                            blurRadius: 7
                          )
                        ]
                      ),
                      child: const Text('Nulla ullamco dolor velit amet sunt ut proident eu laborum. Magna adipisicing qui sint enim ex. Lorem ex cupidatat incididunt anim esse.'),
            ),

            const SizedBox(height: 15.0),

            Row(

              children: [
            
               const SizedBox(width: 40.0), 
               
                Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: Colors.grey),
                  borderRadius: BorderRadius.circular(25)
                ),
                 //color: Colors.red,
                 width: 150.0,
                 height: 45.0,
                 child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children:  [
                    IconButton(
                 onPressed: (){}, 
                 icon: const Icon(Icons.add)
                 ),
                 
                 const Text('1', style: TextStyle(fontSize: 20)),
                 
                 IconButton(
                 onPressed: (){}, 
                 icon: const Icon(Icons.remove)
                 ),
                  ],
                 ),
                ),
            //   ElevatedButton(
            //   style: styleButtonContador,
            //   onPressed: () {
            //   showMyDialog(context);
                 
            //   },
            //   child: const Text("1", style: TextStyle(color: Colors.grey ),),
            // ),
             const SizedBox(width: 20.0),         
                ElevatedButton(
                  style: raiserdButtonStyle,
                  onPressed: () async {
                    Map<String, Object> listaDePedidos = {
                      'id': codigo,
                      'cliente': '${widget.nombreDelCliente}',
                      'producto': producto,
                      'precio': precio,
                      'vendedor': vendedor,
                      'fecha': formatted,
                       //'fecha': currentSelectedDate
                    };
            
                    await _insertProducto(listaDePedidos);
                    getPedidos();
            
            
                  },
                  child: const Text('Agregar', style: TextStyle(color: Color(0xffEFDA30) ),),
                ),
            
                
              ],
            ),

            //const SizedBox(height: 31),

            // ElevatedButton(
            //   style: styleButtonPedidos,
            //   onPressed: () {
            //    const AlertDialogScreen();
                 
            //   },
            //   child: const Text("Ver Pedido", style: TextStyle(color: Color(0xffEFDA30) ),),
            // )
          ],
        ),
      ),
    );
  }

  Future<void> _insertProducto(Map<String, Object> listaDePedidos) async {
    await widget.db!.insert(
      'Pedidos',
      listaDePedidos,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future getPedidos() async {
    lista = await widget.db!.query('Pedidos');
    print('CONTENIDO DE LA LISTA ======= $lista');
    //final  listaproductosSeleccion = lista;

    return lista;
    
  }

}
// class SelectorFecha extends StatefulWidget {
//  // const SelectorFecha({Key? key}) : super(key: key);
//   @override
//   State<SelectorFecha> createState() => _SelectorFechaState();
// }

// class _SelectorFechaState extends State<SelectorFecha> {
//   var currentSelectedDate;
  
//   //1. LLamar al Picker
//   void callDatePicker() async{
//     var selectedDate = await getDatePickerWidget();
//     setState(() {
//       currentSelectedDate = selectedDate;
      
//     });
//   }
  
// Future <DateTime?> getDatePickerWidget() {
//   return showDatePicker(
//     context: context, 
//     initialDate: DateTime.now(), 
//     firstDate: DateTime(2022), 
//     lastDate: DateTime(2023),
//     // builder: (context, child) {
//     //   return Theme(data: ThemeData.dark(), child: child);
//     //   },
//     );
    
//   }
  

// //2.Crear Widget DatePicker.

//   @override
//   Widget build(BuildContext context) {
//     print('Fechaaaaaaaaaa   ${currentSelectedDate}');
//     //3. boton que lanza nuestro dialogo datePicker.
//     return Scaffold(
//         body: Center(
//           child: Container(
//           child: TextButton(
//                   onPressed: callDatePicker, 
//                   child: Text('Seleccionar fecha')),

//           ),
//         ),
//     );
    
//   }
// }