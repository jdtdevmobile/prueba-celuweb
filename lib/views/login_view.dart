//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:project_toro/data/db_manager.dart';
import 'package:project_toro/ui/buttom2_constant..dart';
import 'package:project_toro/ui/inputs_textFields.dart';
import 'package:project_toro/ui/text_button_forget_password.dart';
import 'package:project_toro/views/button_login.dart';
import 'package:sqflite/sqflite.dart';
import 'views.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  String user = "";
  String password = "";
  Database? db;
  DbManager manager = DbManager();
  void llamarDb() async {
    db = await manager.getDatabase;
  }

  @override
  void initState() {
    super.initState();
    manager.initDB();
    llamarDb();
    // crearTablaPedido();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: const Color(0xff00003D),
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ClipRRect(
              child: Image.asset("assets/image/Logo.png"),
            ),
            const SizedBox(
              height: 50.0,
            ),

            TextField(
                style: (const TextStyle(color: Colors.white)),
                autofocus: true,
                onChanged: (value) => {user = value},
                decoration: InputDecorations.authInputDecoration(
                    hintText: 'Usuario',
                    prefixIcon: ClipRRect(
                      child: Image.asset("assets/image/Icono_usuario.png"),
                    ))),

            const SizedBox(height: 10.0),

            TextField(
                style: (const TextStyle(color: Colors.white)),
                autofocus: true,
                obscureText: true,
                onChanged: (value) => {password = value},
                decoration: InputDecorations.authInputDecoration(
                    hintText: 'Contraseña',
                    prefixIcon: ClipRRect(
                      child: Image.asset("assets/image/Icono_contraseña.png"),
                    ))),

            const SizedBox(height: 15.0),

            ForgetPassword(),

            const SizedBox(height: 40),

            // ButtonLogin(
            //   onPressed: () {
                
            //     login();
            //     ;
            //   },
            // )
            //    login();
            // },),

            Container(
              decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(100.0)),
              child: ElevatedButton(
                style: loginButtonStyle2,
                onPressed: () {
                  login();
                },
                child: const Text('Iniciar Sesión',
                    style: TextStyle(
                        color: Color(0xff00003D),
                        fontSize: 18,
                        fontWeight: FontWeight.bold)
                        ),
                    ),
                  )
          ],
        )),
      ),
    ));
  }

  void login() async {
    if (user.isEmpty) {
      print("usuario invalido");
    } else if (password.isEmpty) {
      print("password invalido");
    } else if (db != null) {
      List<Map> usuario = await db!.rawQuery(
          "SELECT * FROM Usuario  WHERE password=$password AND usuario=$user");
      if (usuario.isNotEmpty) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) => VendedoresView(db: db!)));
        print("logueado");
      } else {
        print("contrasena incorrecta");
      }
    }
  }
}
